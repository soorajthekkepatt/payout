<?php

use CodeIgniter\Router\RouteCollection;

use App\Controllers\ClientController;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'Home::index');
$routes->get('/test', 'Home::home');


$routes->match(['get', 'post'], 'login', 'UserController::login', ["filter" => "noauth"]);



// Admin routes
$routes->group("admin", ["filter" => "auth"], function ($routes) {
    $routes->get("/", "AdminController::index");
    $routes->get("home", "AdminController::home");
});


// client routes
$routes->group("client", ["filter" => "auth"], function ($routes) {
    $routes->get("/", "ClientController::index");
    $routes->get("api", "ClientController::apigenerator");
    $routes->get("api/documentation", "ClientController::apidocumentation");
    $routes->post("api/generatetoken", "ClientController::generateapitoken");

});
$routes->get('logout', 'UserController::logout');
