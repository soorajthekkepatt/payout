<?= $this->extend("layouts/client/partials/_header") ?>

<?= $this->section("body") ?>




      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="row flex-grow">
              <div class="col-12 grid-margin stretch-card">
                <div class="card card-rounded">
                  <div class="card-body">
                    
                    <div>
                        <h4 class="card-title card-title-dash">Introduction</h4>
                    <p >Welcome to the Payout S API! You can use our API send payout request for your transactions effortlessly.</p>

                    </div>

                    <div>
                        <h4 class="card-title card-title-dash">Authentication</h4>
                        <p>Payout uses API keys to allow access to the API. If you’re new to Payout, you’ll need to sign up for an account to get your API key. Otherwise, log in and go to Account > API Access to generate a new API token.</p>

                        <p>Payout expects the API key to be included in all API requests to the server using a header like the following:</p>

                        <div class="card-body">
                           
                            <blockquote class="blockquote">
                            <p>Authorization: Token token="9e0cd62a22f451701f29c3bde214"

                                or

                                Authorization: Bearer 9e0cd62a22f451701f29c3bde214</p>
                            </blockquote>
                        </div>



                        
                        
                        <div class="card-body">
                            <blockquote class="blockquote blockquote-primary">
                                <p>You must replace <code>9e0cd62a22f451701f29c3bde214</code> with your personal API key.</p>
                                </footer>
                            </blockquote>
                        </div>
                    </div>

                    

                  
                  </div>
                </div>
              </div>
            </div>
           

            </div>
          </div>


          </div>
        </div>
      </div>
    </div>
  </div>
</div>



<?= $this->endSection() ?>

