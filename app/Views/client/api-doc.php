<?= $this->extend("layouts/client/partials/_header") ?>

<?= $this->section("body") ?>




      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="row flex-grow">
              <div class="col-12 grid-margin stretch-card">
                <div class="card card-rounded">
                  <div class="card-body">
                    
                    <div>
                        <h4 class="card-title card-title-dash">PayOut API Token</h4>
                        <p class="card-subtitle card-subtitle-dash">Keep this token secret and only use it from your backend.</p>
                    </div>

                    <?php if ($token !== null): ?>

                    <div class="card-body">

                   

                        <P><pre ><span id="apitoken"><?php echo $token ?></span><a class="btn btn-default btn-xs copy-button pull-right" data-clipboard-target="#token_c9e51493c2483f3b50701ab97e2d7093" data-original-title="" title=""><button id="copyButton" onclick="copyToClipboard()"><i class="mdi mdi-arrange-send-backward"></i></button></a></pre></p>

                        <div id="copiedMessage" style="display:none;"></div>
                    </div>


                    <?php else: ?>
                      
                    <div class="card-body">

                    <form action="<?= base_url(session()->get('role').'/api/generatetoken') ?>" method="post">
                        <button type="submit" class="btn btn-primary btn-rounded btn-fw">Generate Api Token</button>
                      </form>

                      </div>
                  <?php endif; ?>
                    <p><strong>info:</strong>You can Use this API Token with any of your existing applications.</p>
                  </div>
                </div>
              </div>

              <div>How to use Api ? see <a href="<?php echo base_url(session()->get('role').'/api/documentation'); ?>">documentation !</a></div>
            </div>
           

            </div>
          </div>


          </div>
        </div>
      </div>
    </div>
  </div>
</div>



<?= $this->endSection() ?>

