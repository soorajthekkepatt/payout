
<?= $this->extend("layouts/login/header") ?>


<?= $this->section("body") ?>

          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left py-5 px-4 px-sm-5">
              
              <h4>Hello! let's get started</h4>
              <h6 class="fw-light">Sign in to continue.</h6>

              <?php if (isset($validation)) : ?>
                        <div class="col-12">
                            <div class="alert alert-danger" role="alert">
                                <?= $validation->listErrors() ?>
                            </div>
                        </div>
                    <?php endif; ?>
              <form class="pt-3" action="<?= base_url('login') ?>" method="post">
                <div class="form-group">
                  <input type="email" class="form-control form-control-lg" name="email" id="email"
                    placeholder="Username">
                </div>
                <div class="form-group">
                  <input type="password" class="form-control form-control-lg" name="password" id="password"
                    placeholder="Password">
                </div>
                <div class="mt-3">
                  <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn"
                    >SIGN IN</button>
                </div>
                <div class="my-2 d-flex justify-content-between align-items-center">
                  <div class="form-check">
                    <label class="form-check-label text-muted">
                      <input type="checkbox" class="form-check-input">
                      Keep me signed in
                    </label>
                  </div>
                  <a href="#" class="auth-link text-black">Forgot password?</a>
                </div>

                <div class="text-center mt-4 fw-light">
                  Don't have an account? <a href="#" onclick="alert('Registaration under construction')"  class="text-primary">Create</a>
                </div>
              </form>
            </div>
          </div>


          <?= $this->endSection() ?>

          



