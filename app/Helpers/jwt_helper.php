<?php

use Firebase\JWT\JWT;

if (!function_exists('generate_jwt')) {
    function generate_jwt($data)
    {
        $payload = $data;
        
        $key = 'V2tR8nE#oF!uN7yP@3tZ';

        $algorithm = 'HS256';
        
        $jwt = JWT::encode($payload, $key, $algorithm);
        
        return $jwt;
    }
}