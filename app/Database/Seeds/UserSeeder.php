<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    public function run()
    {
        $user_object = new User();

		$user_object->insertBatch([
			[
				"name" => "user",
				"email" => "user@gmail.com",
				"phone_no" => "7899654125",
				"role" => "client",
				"password" => password_hash("12345678", PASSWORD_DEFAULT)
			],
			[
				"name" => "sooraj",
				"email" => "soorajthekkepatt@gmail.com",
				"phone_no" => "8888888888",
				"role" => "admin",
				"password" => password_hash("12345678", PASSWORD_DEFAULT)
			]
		]);
    }
}
