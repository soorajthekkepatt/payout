<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;

use CodeIgniter\Exceptions\PageNotFoundException;


use App\Models\UserSecretKey;



class ClientController extends BaseController
{
    public function __construct()
    {
        if (session()->get('role') != "client") {
            echo 'Access denied';
            exit;
        }
    }
    public function index()
    {
        return view("client/dashboard");
    }

    public function home()
    {
        return  'hey';
    }

    public function apigenerator($page = 'api-doc'){

        

        if (! is_file(APPPATH . 'Views/client/' . $page . '.php')) {
            // Whoops, we don't have a page for that!
            throw new PageNotFoundException($page);
        }
        
        // return view("client/api-doc");

        $model = model(UserSecretKey::class);

        $token = $model->getToken();
        // print_r($token );
        // exit;

        if(empty($token)){
            $token = null;
        }else{

            $token = $token[0]['secret_key'];
        }


        $data['title'] = ucfirst("api"); // Capitalize the first letter
        $data['token'] = $token;

        // print_r($data );
        // exit;
        
        return view('/client/' . $page, $data);
    }

    function apidocumentation()  {
        
        return view("client/documentation");

    }

    function generateapitoken(){

        helper('jwt');


        $userData = array(
            'user_id' => session()->get('id'),
            'username' => session()->get('name'),            
        );

        // Generate the JWT token
        $token = generate_jwt($userData);

        $model = model(UserSecretKey::class);


        $model->save([
            'user_id' => session()->get('id'),
            'secret_key'  => $token,
        ]);


        $token = $model->getToken();
        $data['token'] = $token[0]['secret_key'];
        return view('/client/api-doc', $data);

    }
}
