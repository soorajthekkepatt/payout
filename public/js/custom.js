function copyToClipboard() {
    // Get the text content of the span
    var spanText = document.getElementById('apitoken').innerText;

    // Create a temporary textarea element and set its value to the span text
    var tempTextArea = document.createElement('textarea');
    tempTextArea.value = spanText;

    // Append the textarea to the document
    document.body.appendChild(tempTextArea);

    // Select the text inside the textarea
    tempTextArea.select();

    // Execute the copy command
    document.execCommand('copy');

    // Remove the temporary textarea
    document.body.removeChild(tempTextArea);

    // Display the copied message
    var copiedMessage = document.getElementById('copiedMessage');
    copiedMessage.innerText = 'Successfully copied API Token ';
    copiedMessage.style.display = 'block';

    // Hide the message after 2 seconds
    setTimeout(function () {
        copiedMessage.style.display = 'none';
    }, 4000);
}